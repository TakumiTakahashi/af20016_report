import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.applet.*;
public class VendingMachine {

    private JPanel root;
    private JButton colaButton;
    private JButton greenteaButton;
    private JButton waterButton;
    private JButton coffeeButton;
    private JButton orangeButton;
    private JButton milkteaButton;
    private JTextPane Itemlist;
    private JButton checkoutButton;
    private JLabel Totalyen;
    private JLabel metor;
    private AudioClip clip;

    public static void main(String[] args) {
        JFrame frame = new JFrame("VendingMachine");
        frame.setContentPane(new VendingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        new VendingMachine();
    }

    int yen=0;
    int honsuu = 0;


    int price(String drink){
        if(drink == "Cola") {
            return 160;
        }
        else if(drink == "GreenTea"){
            return 130;
        }
        else if(drink == "Water"){
            return 100;
        }
        else if(drink == "Coffee"){
            return 140;
        }
        else if(drink == "OrangeJuice"){
            return 150;
        }
        else if(drink == "MilkTea"){
            return 300;
        }
        return 0;
    }

    void order(String drink) {
        clip = Applet.newAudioClip(getClass().getResource("se_sad07.wav"));
        clip.play();
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + drink,
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            honsuu = honsuu + 1;

            yen += price(drink);
            Totalyen.setText(yen + " yen");
            String currentText = Itemlist.getText();
            Itemlist.setText(currentText + drink + " " + price(drink) + "yen"  + "\n");
            JOptionPane.showMessageDialog(null, "Order for " + drink + " received.");

            if(honsuu == 1)
                metor.setIcon(new ImageIcon( this.getClass().getResource("1.png")));
            if(honsuu == 2)
                metor.setIcon(new ImageIcon( this.getClass().getResource("2.png")));
            if(honsuu == 3)
                metor.setIcon(new ImageIcon( this.getClass().getResource("3.png")));
            if(honsuu == 4)
                metor.setIcon(new ImageIcon( this.getClass().getResource("4.png")));
            if(honsuu == 5)
                metor.setIcon(new ImageIcon( this.getClass().getResource("5.png")));
            if(honsuu == 6)
                metor.setIcon(new ImageIcon( this.getClass().getResource("6.png")));
            if(honsuu == 7)
                metor.setIcon(new ImageIcon( this.getClass().getResource("7.png")));
            if(honsuu == 8)
                metor.setIcon(new ImageIcon( this.getClass().getResource("8.png")));
            if(honsuu == 9)
                metor.setIcon(new ImageIcon( this.getClass().getResource("9.png")));

            if (honsuu == 10) {
                metor.setIcon(new ImageIcon( this.getClass().getResource("10.png")));
                clip = Applet.newAudioClip(getClass().getResource("se_sod03.wav"));
                clip.play();
                JOptionPane.showMessageDialog(null, "1 drink free!");
                int Bounus = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to order free " + drink,
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (Bounus == 0) {
                    Totalyen.setText(yen + " yen");
                    String free = Itemlist.getText();
                    Itemlist.setText(free + drink + " " + "free"  + "\n");
                    JOptionPane.showMessageDialog(null, "Order for free " + drink + " received.");
                    honsuu = 0;
                    metor.setIcon(new ImageIcon( this.getClass().getResource("0.png")));

                }

            }
        }
    }



    public VendingMachine() {

        metor.setIcon(new ImageIcon( this.getClass().getResource("0.png")));

        colaButton.setIcon(new ImageIcon( this.getClass().getResource("cola.png")));
        greenteaButton.setIcon(new ImageIcon( this.getClass().getResource("greentea.png")));
        waterButton.setIcon(new ImageIcon( this.getClass().getResource("water.png")));
        coffeeButton.setIcon(new ImageIcon( this.getClass().getResource("coffee.png")));
        orangeButton.setIcon(new ImageIcon( this.getClass().getResource("orange.png")));
        milkteaButton.setIcon(new ImageIcon( this.getClass().getResource("milktea.png")));

        colaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cola");
            }
        });
        greenteaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("GreenTea");
            }
        });
        waterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Water");

            }
        });
        coffeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coffee");

            }
        });
        orangeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("OrangeJuice");

            }
        });
        milkteaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("MilkTea");

            }
        });
        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clip = Applet.newAudioClip(getClass().getResource("se_sad07.wav"));
                clip.play();
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cheakout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ yen + " yen.");
                    Itemlist.setText(" ");
                    yen = 0;
                    Totalyen.setText(yen+" yen");
                }
                clip = Applet.newAudioClip(getClass().getResource("se_moc08.wav"));
                clip.play();
            }
        });
    }
}
